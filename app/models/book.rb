class Book < ActiveRecord::Base


  def self.all_genres ; %w[Science\ fiction Drama Action\ and\ romance Romance Mystery Horror] ; end

  validates :genre, :inclusion => {:in => Book.all_genres }


    def self.similar_books(book)
        Book.where author: book.author
    end
end
