# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
more_books = [
 {:title => 'Under the Dome', :genre => 'Horror', :description => 'A town is inexplicably and suddenly sealed off from the rest of the world', :isbn_number => '1439149038', :publish_date => '26-Jul-2010' },
 {:title => 'Metro 2033', :genre => 'Horror', :description => 'In post-war nuclear apocalypse humanity is nearly extinct, living in the subways of Moscow', :isbn_number => '0575086254', :publish_date => '18-Mar-2010' },
 {:title => 'Futu.re', :genre => 'Science fiction', :description => 'Discoveries made within our lifetime will allow people to remain young forever. There is no more death. Every utopia has its shadowy backstreets.', :isbn_number => '1517679273', :publish_date => '05-Oct-2015' },
 {:title => 'The Island of Doctor Moureau', :genre => 'Science fiction', :description => 'A shipwrecked man rescued by a passing boat is rescued by a passing boat and left of the island home of doctor Moureau, a mad scientist who creates human-like hybrid beings from animals via vivisection', :isbn_number => '0553214322', :publish_date => '01-May-1994' },
 {:title => 'Witcher: The Last Wish', :genre => 'Action and romance', :description => 'Geralt is witcher, a man whose magic powers, enhanced by logs training and a mysterious elixir, have made him a brilliant fighter and a merciless assassin.', :isbn_number => '0575082445', :publish_date => '14-Feb-2008' }
]
more_books.each do |book|
 Book.create!(book)
end
